import React, { Component } from 'react'
import { Route, Link } from "react-router-dom";
import Home from './../Screens/Home'
import Login from './../Screens/Login'


export default class MyRouters extends Component {
    render() {
        return (
            <div>
                <Route exact path="/chat/:groupId" component={Home} />
                <Route exact path="/" component={Login} />
            </div>
        )
    }
}
