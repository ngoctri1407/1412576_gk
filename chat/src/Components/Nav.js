import React, { Component } from 'react';
import firebase from "firebase";
import history from '../history';

class Nav extends Component {
    constructor(props) {
        super(props);
    }

    _signOut(){
        firebase.auth().signOut().then(function() {
            console.log("logout")
            history.push("/");
        }).catch(function(error) {
            // An error happened.
        });
    }

    render() {
        return (
            <div className="App">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a onClick={()=>history.push("/chat/public")} className="navbar-brand" href="#">HCMUS Chat App</a>
                    <button onClick={()=>this._signOut()} type="button" className="btn btn-secondary float-right">Logout</button>
                </nav>

            </div>
        )
    }
}
export default Nav;