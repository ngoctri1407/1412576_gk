import React, { Component } from 'react';
import {connect} from "react-redux";
import history from "../../history";
import * as Actions from "../../Actions/auth";

class People extends Component {
    constructor(props){
        super(props);
    }
    _onclick(userId){
        this.props.dispatch(Actions.doChangePartnerName(this.props.name));
        let roomId = this.props.oUser.uid[0]<userId[0]?this.props.oUser.uid + userId:userId+this.props.oUser.uid;
        history.push("/chat/"+roomId);
    }

    render (){
    return (
        <li onClick={()=>this._onclick(this.props.userId)}  className="clearfix">
            <img style={{maxWidth:45,borderRadius:50}} src={this.props.avatar} alt="avatar" />
            <div className="about">
                <div className="name">{this.props.name}</div>
                <div className="status">
                    <i className={ "fa fa-circle " + this.props.status}/>{this.props.status !== 'offline'?"online":`left ${this.props.minuteOffline} mins ago`}
                </div>
            </div>
        </li>
    )
    }
}

const mapStateToProps = ({firebase}) => {
    return {
        oUser:firebase.auth
    };
};

export default connect(mapStateToProps, null)(People);
