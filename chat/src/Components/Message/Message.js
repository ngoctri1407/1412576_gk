import React, { Component,Fragment } from 'react';
import {connect} from "react-redux";


class Message extends Component {
    constructor(props){
        super(props);
    }


    isMe(){
        if(this.props.sendId ===this.props.oUser.uid)
        return true;
        return false;
    }

    render (){
    return (
            <li className={"clearfix"}>
                <div className={this.isMe()?"message-data align-right":"message-data"}>
                    <span className="message-data-time">{this.props.sendAt}</span> &nbsp; &nbsp;
                    {this.isMe()?
                        <Fragment><span className="message-data-name">{this.props.name}</span> <i className="fa fa-circle me"/></Fragment>
                        :<Fragment><span className="message-data-name"><i className="fa fa-circle online"/> {this.props.name}</span></Fragment>}
                </div>
                <div className={this.isMe()?"message other-message float-right":"message my-message"}>
                    {this.props.message.type==="message"?this.props.message.text:(<a href={this.props.message.text}>{this.props.message.text}<img className="image-chat" src={this.props.message.text}/></a>)}
                </div>
            </li>
    )
    }
}
const mapStateToProps = ({firebase}) => {
    return {
        oUser:firebase.auth
    };
};

export default connect(mapStateToProps, null)(Message);
