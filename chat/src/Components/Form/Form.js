import React, { Component,Fragment } from 'react';
import Message from '../Message/Message';
import firebase from 'firebase';
import {connect} from "react-redux";
import * as ReactDOM from 'react-dom';
import FileUploader from "react-firebase-file-uploader";

class Form extends Component {
    constructor(props){
        super(props);
        this.state = {
            userName: 'Sebastian',
            message: '',
            list: [],
            groupId:'public'
        };
    }
    scrollToBottom = () => {
        const { messageList } = this.refs;
        const scrollHeight = messageList.scrollHeight;
        const height = messageList.clientHeight;
        const maxScrollTop = scrollHeight - height;
        ReactDOM.findDOMNode(messageList).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.groupId !== this.state.groupId){
            this.messageRef = firebase.database().ref().child(`groups/${nextProps.groupId}/messages`);
            this.setState({
                groupId:nextProps.groupId,
                list:[]
            });
            this.listenMessages();
        }
    }
    componentWillMount(){
        this.messageRef = firebase.database().ref().child(`groups/${this.props.groupId}/messages`);
        this.listenMessages();
    }
    handleChange(event) {
        this.setState({message: event.target.value});
    }
    handleSend() {
        let type = 'message';
        const extensionIndex = this.state.message.lastIndexOf(".");
        const extension = this.state.message.slice(extensionIndex + 1);
        const allowedExtensions = ["jpg", "jpeg", "png"];
        if (allowedExtensions.includes(extension)) {
            type = 'image'
        }

        if (this.state.message) {
            var newItem = {
                userId:this.props.oUser.uid,
                avatar:this.props.oUser.photoURL,
                name: this.props.oUser.displayName,
                message: {
                    text:this.state.message,
                    type:type
                },
                sendAt:new Date().toDateString()
            }
            this.messageRef.push(newItem);
            this.setState({ message: '' });
        }
    }
    handleKeyPress(event) {
        if (event.key !== 'Enter') return;
        this.handleSend();
    }
    listenMessages() {
        this.messageRef
            .limitToLast(10)
            .on('value', message => {
                if(message.val())
                this.setState({
                    list: Object.values(message.val()),
                });
            });
    }
    componentDidUpdate(){
        this.scrollToBottom();
    }
    handleUploadSuccess = filename => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref("images")
            .child(filename)
            .getDownloadURL()
            .then(url => {
                var newItem = {
                    userId:this.props.oUser.uid,
                    avatar:this.props.oUser.photoURL,
                    name: this.props.oUser.displayName,
                    message: {
                        text:url,
                        type:'image'
                    },
                    sendAt:new Date().toDateString()
                }
                this.messageRef.push(newItem);
                this.setState({ message: '' });
            });
    };

    render (){
        return (
            <div style={{height:850}} className="chat">
                <div className="chat-header clearfix">
                    <img style={{maxWidth:50}} src={this.state.groupId ==='public'?"https://www.eovietnam.com/wp-content/uploads/2016/09/logo-khtn.png":"https://www.eovietnam.com/wp-content/uploads/2016/09/logo-khtn.png"}
                         alt="avatar"/>

                    <div className="chat-about">
                        <div className="chat-with">{this.state.groupId ==='public'?'Phòng chat công cộng':'Chat với '+this.props.partnerName}</div>
                    </div>
                </div>

                <div style={{overflowX:'hidden'}} ref="messageList" className="chat-history">
                    <ul>
                        {this.state.list.map(item=>
                            <Message
                                sendId={item.userId}
                                name={item.name}
                                message={item.message}
                                sendAt={item.sendAt}
                            />
                        )}

                    </ul>

                </div>
                <div className="chat-message clearfix">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label style={{backgroundColor: 'steelblue', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor'}}>
                                Send an image
                                <FileUploader
                                    hidden
                                    accept="image/*"
                                    storageRef={firebase.storage().ref('images')}
                                    onUploadSuccess={this.handleUploadSuccess}
                                />
                            </label>
                        </div>
                        <input type="text"
                               placeholder="Type message"
                               value={this.state.message}
                               onChange={this.handleChange.bind(this)}
                               onKeyPress={this.handleKeyPress.bind(this)}
                               className="form-control"  aria-label="" />
                    </div>
                    <button onClick={()=>this.handleSend()}>Send</button>

                </div>
            </div>

        )
    }
}
const mapStateToProps = ({firebase,Auth}) => {
    return {
        oUser:firebase.auth,
        partnerName:Auth.partnerName
    };
};

export default connect(mapStateToProps, null)(Form);
