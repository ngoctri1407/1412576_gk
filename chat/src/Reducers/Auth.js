import * as Actions from "./../Actions";
import _ from "lodash";
import {toast} from "react-toastify";

const defaultState = {
    partnerName:""
};
export default function (state = defaultState, action) {
    let _newState;
    switch (action.type) {
        case 'CHANGE_PARTNER_NAME':
            return {
                ...state,
                partnerName:action.payload,
            };
        default:
            return {
                ...state,
                error: ""
            };
    }
}
