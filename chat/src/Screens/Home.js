import React, {Component} from 'react';
import '../App.css';
import {connect} from "react-redux";
import './Home.scss';
import People from '../Components/People/People';
import Message from '../Components/Message/Message';
import Form from '../Components/Form/Form';
import firebase from "firebase";

class Home extends Component {
    constructor(props) {
        super(props);
        this.usersRef = firebase.database().ref().child('users');
        this.listenUsers();
        this.state={
            listUser:[],
            groupId:'public',
            searchString:'',
            listUserSearch:[]
        }
    }

    componentWillMount(){


    }

    listenUsers() {
        this.usersRef
            .limitToLast(100)
            .on('value', message => {
                if(message.val())
                    this.setState({
                        listUser: Object.values(message.val()),
                        listUserSearch:Object.values(message.val())
                    });
            });
    }
    componentDidUpdate(nextProps) {
        if(this.props.match.params.groupId !== this.state.groupId){
            this.setState({
                groupId:this.props.match.params.groupId
            })
        }
    }
    handleChange(event) {
        this.setState({searchString: event.target.value},()=>{
            let searchArray = [...this.state.listUser];
            this.setState({listUserSearch:searchArray.filter(user => user.name.includes(this.state.searchString))});
        });
    }
    render() {
        return (
            <div className="App">
                <div className="container clearfix">
                    <div className="people-list" id="people-list">
                        <div className="search">
                            <input value={this.state.searchString} onChange={this.handleChange.bind(this)} type="text" placeholder="search"/>
                            <i className="fa fa-search"/>
                        </div>
                        <ul className="list">
                            {this.state.listUserSearch.map(item=>
                                <People avatar={item.avatar}
                                        name={item.name}
                                        status={'online'}
                                        userId={item.id}
                                        key={item.userId}
                                />
                            )}
                        </ul>
                    </div>

                    <Form groupId={this.state.groupId}/>

                </div>
            </div>
        );
    }
}

const mapStateToProps = ({firebase}) => {
    return {
        oUser:firebase.auth
    };
};

export default connect(mapStateToProps, null)(Home);