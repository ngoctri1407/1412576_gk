import React, { Component } from 'react';
import '../App.css';
import firebase from "firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import { connect } from "react-redux";
import {Bounce} from 'react-activity';

class Login extends Component {
    constructor(props){
        super(props);
        this.state = { isSignedIn: false }
        this.usersRef = firebase.database().ref().child('users');
    }

    uiConfig = {
        signInFlow: "popup",
        signInOptions: [
            firebase.auth.GoogleAuthProvider.PROVIDER_ID
        ],
        callbacks: {
            signInSuccess: (user)=>this._onLoginSuccess(user)
        }
    };


    _onLoginSuccess(user){
        const context = this;
        var newItem = {
            name: user.displayName,
            avatar: user.photoURL,
            id:user.uid
        }
        firebase.database().ref().child("users").orderByChild("id").equalTo(user.uid).on("value", function(snapshot) {
            if (snapshot.exists()) {
                console.log("exists");
            }else{
                context.usersRef.push(newItem);
            }
        });
        this.props.history.push("/chat/public");
    }
    componentDidMount(){
        firebase.auth().onAuthStateChanged(user => {
            this.setState({ isSignedIn: !!user })
        })
    }






    render() {
        return (
            <div className="App">

                {this.state.isLoading ? (
                    <Bounce color="#727981" size={32} speed={1} animating={true} />
                ) : (
                    <StyledFirebaseAuth
                        uiConfig={this.uiConfig}
                        firebaseAuth={firebase.auth()}
                    />
                )}
            </div>
        );
    }
}
export default Login;