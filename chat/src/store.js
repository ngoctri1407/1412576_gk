import { Provider } from 'react-redux'
import { createStore, combineReducers, compose,applyMiddleware } from 'redux'
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase'
import firebase from 'firebase'
import createSagaMiddleware from "redux-saga";
import logger from "redux-logger";
import rootSaga from './Saga';
import _rootReducer from './Reducers'
const sagaMiddleware = createSagaMiddleware();

const firebaseConfig = {
    apiKey: "AIzaSyBYXufdHuE2r7RS58m08ZtV1VN84OjYmIg",
    authDomain: "school-bt2.firebaseapp.com",
    databaseURL: "https://school-bt2.firebaseio.com",
    projectId: "school-bt2",
    storageBucket: "school-bt2.appspot.com",
    messagingSenderId: "3453287012"
};

// react-redux-firebase config
const rrfConfig = {
    userProfile: 'users'
}

firebase.initializeApp(firebaseConfig)
const middlewares = [sagaMiddleware,logger];
// Add reactReduxFirebase enhancer when making store creator
const createStoreWithFirebase = compose(
    applyMiddleware(...middlewares),
    reactReduxFirebase(firebase, rrfConfig)
)(createStore)

// Add firebase to reducers
const rootReducer = combineReducers({
    firebase: firebaseReducer,
    ..._rootReducer
})

if (process.env.NODE_ENV === "development") {
    middlewares.push(logger);
}

// Create store with reducers and initial state
const initialState = {}
const store = createStoreWithFirebase(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
store.runSaga = sagaMiddleware.run;

store.runSaga(rootSaga);
export default store;