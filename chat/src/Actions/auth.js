export const CHANGE_PARTNER_NAME = "CHANGE_PARTNER_NAME";

export function doChangePartnerName(payload) {
  return {
    type: CHANGE_PARTNER_NAME,
    payload
  };
}
