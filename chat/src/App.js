import React, { Component,Fragment } from 'react';
import './App.css';
import Home from './Screens/Home';
import {Provider} from 'react-redux';
import MyRouter from './router/MyRouters'
import { Router } from 'react-router-dom';
import Nav from './Components/Nav';
import history from './history';

import store from './store';
class App extends Component {


  render() {
    return (
        <Provider store={store}>
            <Fragment>
                <Nav/>
                <Router history={history}>
                <MyRouter/>
                </Router>
            </Fragment>
        </Provider>
    );
  }
}

export default App;
